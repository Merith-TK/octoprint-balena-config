#!/bin/bash
get_latest_release() {
  curl --silent "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
    grep '"browser_download_url":' | 
    grep 'linux' |
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}
balena_download=$(get_latest_release balena-io/balena-cli)

pushd /workspace/
echo "Downloaded Balena-CLI"
wget $balena_download
unzip balena-cli*
export PATH=$PATH:/workspace/balena-cli
popd